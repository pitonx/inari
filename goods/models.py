from django.db import models
from django.core.urlresolvers import reverse


class Good(models.Model):
    """Модель товара"""
    name = models.CharField(max_length=50, db_index=True, verbose_name='Название продукта')
    preview_description = models.CharField(max_length=200, blank=True, verbose_name='Краткое описание')
    description = models.TextField(blank=True, verbose_name='Полное описание')
    price = models.DecimalField(max_digits=15, decimal_places=2, verbose_name='Цена')
    active = models.BooleanField(default=True, verbose_name="Активный")
    created = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')

    class Meta:
        ordering = ['name']
        verbose_name = 'Товар'
        verbose_name_plural = 'Товары'

    def __str__(self):
        return self.name


class Order(models.Model):
    """Модель заказов"""
    good = models.ForeignKey(Good, verbose_name='Товар')
    user = models.ForeignKey('auth.User', verbose_name='Покупатель')
    total = models.DecimalField(max_digits=15, decimal_places=2, verbose_name='Стоимость')
    created = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')

    class Meta:
        ordering = ['created']
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'

    def __str__(self):
        return 'Заказ: {}'.format(self.id)
