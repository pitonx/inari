from django.contrib import admin
from .models import Good, Order


class GoodAdmin(admin.ModelAdmin):
    list_display = ['name', 'preview_description', 'description', 'price', 'created', 'active']
    list_editable = ['preview_description', 'price', 'active']


admin.site.register(Good, GoodAdmin)
admin.site.register(Order)
