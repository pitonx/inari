from django.conf.urls import url, handler404
from django.contrib.auth import views as auth_views
from . import views

# редиректим все на главную
# handler404 = views.good_list

urlpatterns = [
    url(r'^$', views.good_list, name='good_list'),
    url(r'^login/$', views.login, name='login'),
    url(r'^logout/$', views.logout, name='logout'),
    url(r'^good/(?P<id>\d+)/order/$', views.order, name='order'),
    url(r'^orders/$', views.order_list, name='order_list'),
    url(r'^good/new/$', views.good_new, name='good_new'),
    url(r'^good/(?P<id>\d+)/edit/$', views.good_edit, name='good_edit'),
    url(r'^good/(?P<id>\d+)/del/$', views.good_del, name='good_del'),
    url(r'^good/(?P<id>\d+)$', views.good_detail, name='good_detail')
]
