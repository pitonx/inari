import sys
from django.contrib import auth
from django.shortcuts import render, get_object_or_404, redirect
from django.utils import timezone
from django.contrib.auth.decorators import login_required

from .models import Good, Order
from .forms import GoodForm


def good_list(request):
    """Список товаров"""
    if request.user.is_superuser:
        goods = Good.objects.all()
        title = 'Админка'
    else:
        goods = Good.objects.filter(active=True)
        title = 'Список товаров'
    return render(request, 'goods/list.html', {'goods': goods, 'title': title, 'title_list': 'Продукты'})


def good_detail(request, id):
    """Детальная страница товара"""
    good = get_object_or_404(Good, id=id)
    return render(request, 'goods/detail.html', {'good': good})


def good_new(request):
    """Новый товар"""
    if not request.user.is_superuser:
        return render(request, 'goods/error.html', {'message_error': 'Недостаточно прав для совершения операции'})

    if request.method == 'POST':
        form = GoodForm(request.POST)
        if form.is_valid():
            try:
                good = form.save(commit=False)
                good.author = request.user
                good.created = timezone.now()
                good.save()
            except:
                return render(request, 'goods/error.html', {'message_error': sys.exc_info()[0]})
            return redirect('goods:good_detail', id=good.id)
    else:
        form = GoodForm()
        return render(request, 'goods/good_edit.html', {'form': form})
    return redirect('/')


def good_edit(request, id):
    """Редактирование товара"""
    good = get_object_or_404(Good, id=id)
    if request.method == 'POST':
        form = GoodForm(request.POST, instance=good)
        if form.is_valid():
            try:
                form.save()
            except:
                return render(request, 'goods/error.html', {'message_error': sys.exc_info()[0]})
            return redirect('goods:good_detail', id=good.id)
    else:
        form = GoodForm(instance=good)
    return render(request, 'goods/good_edit.html', {'form': form, 'good': good})


def good_del(request, id):
    """Удаление товара"""
    if not request.user.is_superuser:
        return render(request, 'goods/error.html', {'message_error': 'Недостаточно прав для совершения операции'})

    if not Good.objects.filter(id=id).exists():
        return render(request, 'goods/error.html', {'message_error': 'Не найден объект с id={0}'.format(id)})

    Good.objects.filter(id=id).delete()
    return redirect('/')


def order_list(request):
    """Список заказов"""
    orders = Order.objects.all()
    return render(request, 'goods/list.html', {'orders': orders, 'title': 'Список заказов', 'title_list': 'Заказы'})


# @login_required()
def order(request, id):
    """Сохраняет заказ"""
    if not request.user.is_authenticated():
        return render(request, 'goods/error.html', {'message_error': 'Пользователь не авторизован'})

    good = get_object_or_404(Good, id=id)
    order = Order()
    order.good = good
    order.user = request.user
    order.total = good.price

    try:
        order.save()
    except:
        return render(request, 'goods/error.html', {'message_error': sys.exc_info()[0]})

    return redirect('/')


def login(request):
    """Авторизация пользователя"""
    if ('username' not in request.POST) or ('password' not in request.POST):
        return render(request, 'goods/error.html', {'message_error': 'Не задан логин или пароль'})

    username = request.POST['username']
    password = request.POST['password']
    user = auth.authenticate(username=username, password=password)
    if user is not None and user.is_active:
        auth.login(request, user)
        return redirect('/')
    else:
        return render(request, 'goods/error.html', {'message_error': 'Ошибка авторизации'})


def logout(request):
    """Выход"""
    auth.logout(request)
    return redirect('/')
